            /'\
     /\    /   \    /\
    /  \__/     \__/  \
   /                   \
  / Shapes Library v2.5 \
 |                       |
 ._  by Salomon Zwecker _.
   \   - - 2009  - -   / 
    '*| BSD License |*'
      +-------------+
   ,./|   README    |\.,
-,.=====================.,- +
     http://there-it-is.com | '
 http://shapes.codeplex.com |   ' 
~~+=====================+~~ +     '

  Table Of Contents
    1. Setting Up The Library
    // 2. About The Sampler Library
    


  -'*~~~~~~~~~~~~~~~~~~~~~~*'-
  1. Setting Up The Library
  -,.======================.,-

i. Finding The Shapes DLLs
  a. if you downloaded the binary version: 
          - extract the zip file to somewhere on your hard-disk.
          - navigate to the dll-folder. 
  b. if you downloaded (or checked out) the source code version:
          - open the solution for the xna version you wish
          - make sure that the build mode is set to "Release"
          - right click onto the Shapes project (Windows, Xbox and/or Zune)
          - select "Build" in the drop down
          - Navigate to the Folder where the solution files are.
            Then navigate to ".\bin\Xna3.#\[target platform]\Release"

ii. Referencing The DLLs
  - copy the Shapes.Windows.dll, Shapes.Xbox.dll and/or Shapes.Zune.dll as well as the Shapes.xml files.
  - [if you wanna use it with a new xna project, create one]
  - paste them inside the folder structure of your project (not in a bin folder).
    it is a good idea to create a folder where all external libraries are located.
  - open your xna project with visual studio and right click on the "References" folder
  - Click on "Add References..."
  - Select the "Browse" tab and navigate to the folder where you copied the dlls before
  - Select the dll for the corresponding target platform and press the OK Button
  -> do this for every of your projects where you need to use Shapes.

iii. Namespaces
  - To use something of the library use the following namespaces:
  
    * Shapes.Geometry
        -> here are all Shapes and Drawings as well as the transformation class
    * Shapes.Misc
        -> this namespace provides collision detection and diplaying a geometry
    * Shapes.Misc.Appearance
        -> here is the brush (needed to draw a shape's border)
  
  - write "using [namespace];" on top of a class where you wanna use the functionality of one namespace
  - for further information about the classes please visit http://shapes.codeplex.com/ or take a look at the samples.



  -'*~~~~~~~~~~~~~~~~~~~~~~~~~*'-
  2. About The Sampler Library
  -,.=========================.,-

The Sampler Library has nothing to do with the Shapes Library. You don't need it to use Shapes.
It just manages the samples of the library.
The Sampler Library has been encapsulated to hide the not relevant basic stuff from the user.
So the user can focus the important things: how to interact with the library the sample is for.
You only need the Sampler Project, if you wanna make some samples for your own library.
You shouldn't use it to make a game with it.