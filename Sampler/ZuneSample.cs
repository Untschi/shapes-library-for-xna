﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sampler
{
    /// <summary>
    /// The ZuneSample changes the resolution to the resolution of a Zune.
    /// When Adding a Sample to the SampleManager at a Zune-Startup project, it is only added if it is a ZuneSample.
    /// </summary>
    public abstract class ZuneSample : Sample
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ZuneSample"/> class.
        /// </summary>
        /// <param name="description">the description which will be displayed while the sample is active</param>
        protected ZuneSample(string description)
            : base(description)
        {
#if !(XBOX || XBOX360)
            SetResolution(240, 320);
#endif
        }

    }
}
