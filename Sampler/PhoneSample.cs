using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sampler
{
    /// <summary>
    /// The PhoneSample changes the resolution to the resolution of a Zune.
    /// When Adding a Sample to the SampleManager at a Phone-Startup project, it is only added if it is a PhoneSample.
    /// </summary>
    public abstract class PhoneSample : Sample
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PhoneSample"/> class.
        /// </summary>
        /// <param name="horizontal">indicating if the screen is in landscape mode (true) or portrait (false)</param>
        /// <param name="description">the description which will be displayed while the sample is active</param>
        protected PhoneSample(bool horizontal, string description)
            : base(description)
        {
#if !(XBOX || XBOX360)
            if (horizontal)
                SetResolution(800, 480);
            else
                SetResolution(480, 800);
#endif
        }

    }
}
