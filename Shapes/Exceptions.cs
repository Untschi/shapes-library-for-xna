﻿/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes
{
    /// <summary>
    /// Exception is thrown when a polygon or B-Spline has less than 3 points
    /// </summary>
    public class TooFewPointsException : Exception
    {
        /// <summary>
        /// Creates a new TooFewPointException
        /// </summary>
        /// <param name="message">the message of the exception</param>
        public TooFewPointsException(string message)
            : base(message)
        {
        }
    }

    /// <summary>
    /// Exception is thrown when a SpriteShape or FillTexture is created but the ShapeTexturePool hasn't been initialized before
    /// </summary>
    public class ShapeTexturePoolNotInitializedException : Exception
    {
        /// <summary>
        /// Creates a new ShapeTexturePoolNotInitializedException
        /// </summary>
        public ShapeTexturePoolNotInitializedException()
            : base("You have to call the static method ShapeTexturePool.Initialize(content) one time before creating SpriteShapes or FillTextures")
        {
        }
    }
}
