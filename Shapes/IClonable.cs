using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Geometry;

namespace Shapes
{
    public interface ICloneable
    {
        object Clone();
    }
}
