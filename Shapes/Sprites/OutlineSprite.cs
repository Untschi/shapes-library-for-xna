﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Geometry;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Shapes.Sprites
{
    public enum OutlineTextureMode
    {
        /// <summary>
        /// The whole outline texture is stretched to the size of each line of the geometries.
        /// </summary>
        LineStretch = 0,
        /// <summary>
        /// The whole outline texture is stretched to the whole geometry (start touching end).
        /// </summary>
        GeometryStretch = 1,
        /// <summary>
        /// The outline texture keep its size and is tiled.
        /// </summary>
        OriginalSize = 3,
    }

    public class OutlineSprite
    {
        public OutlineTextureMode TextureStrechMode { get; set; }

        public ILinedGeometry Geometry { get; set; }

        public Texture2D OutlineTexture { get; set; }

        public float TextureScale { get; set; }
        public Vector2 SegmentOrigin { get; set; }
        public Color Color { get; set; }

        public bool UseLocalCoordinates { get; set; }

        public OutlineSprite(Texture2D texture, ILinedGeometry geometry)
        {
            this.OutlineTexture = texture;
            this.Geometry = geometry;
            this.TextureStrechMode = OutlineTextureMode.LineStretch;
            this.TextureScale = 1;
            this.SegmentOrigin = new Vector2(0.5f * texture.Height, 0.5f * texture.Height);
            this.Color = Color.White;
            this.UseLocalCoordinates = geometry is Line;
        }

        public void Draw(SpriteBatch batch)
        {
            switch (TextureStrechMode)
            {
                case OutlineTextureMode.LineStretch:

                    foreach (Line l in Geometry.GetLines())
                    {
                        Vector2 pos = (UseLocalCoordinates) ? l.StartPoint : Geometry.Transform.TransformLocalToGlobal(l.StartPoint);

                        Rectangle outerRect = new Rectangle(0, 0, (int)SegmentOrigin.X, OutlineTexture.Height);
                        DrawSegment(batch, l, pos, outerRect, SegmentOrigin, true);

                        Rectangle sourceRect = new Rectangle((int)SegmentOrigin.X, 0, (int)(OutlineTexture.Width - 2 * SegmentOrigin.X), OutlineTexture.Height);
                        DrawSegment(batch, l, pos, sourceRect, new Vector2(0, SegmentOrigin.Y), false);

                        outerRect.X = (int)(OutlineTexture.Width - SegmentOrigin.X);
                        DrawSegment(batch, l, pos, outerRect, new Vector2(-l.BorderLength + 1, SegmentOrigin.Y), true);
                    }

                    break;
                case OutlineTextureMode.GeometryStretch:

                    throw new NotImplementedException();
                    break;
                case OutlineTextureMode.OriginalSize:

                    float texPos = 0;
                    throw new NotImplementedException();
                    break;
            }
        }

        private void DrawSegment(SpriteBatch batch, Line line, Vector2 position, Rectangle sourceRectangle, Vector2 origin, bool UseSourceRectSize)
        {
            Rectangle rect = new Rectangle((int)(position.X), (int)(position.Y),
                UseSourceRectSize ? sourceRectangle.Width : (int)line.BorderLength, 
                 (int)(TextureScale * (UseSourceRectSize ? sourceRectangle.Height : OutlineTexture.Height)));

            Vector2 tangent = line.EndPoint - line.StartPoint;
            float rotation = (float)Math.Atan2(tangent.Y, tangent.X);

            batch.Draw(OutlineTexture, rect, sourceRectangle, Color, rotation + Geometry.Transform.Rotation.ClockwiseRadians, origin, SpriteEffects.None, 0);
        }

    }
}
