﻿/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Shapes.Misc.Appearance
{

    /// <summary>
    /// Derivations of the BrushMask class can modify the appearance and form of a brush line
    /// </summary>
    public abstract class BrushMask
    {
        internal abstract void ModifyScaleFactor(ref Vector2 scale);

        internal abstract float GetVisibilityValue(Brush brush, Drawing geometry, ref Vector2 localPosition, ref float distance, ref float? t);
    }
}
