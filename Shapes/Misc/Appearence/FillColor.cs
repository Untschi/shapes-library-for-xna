﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Shapes.Misc.Appearance
{
    /// <summary>
    /// A Fill Object which only provides one color
    /// </summary>
    public sealed class FillColor : Fill
    {
        /// <summary>
        /// The Fill Color
        /// </summary>
        public Color Color { get; set; }

        /// <summary>
        /// Creates a new FillColor
        /// </summary>
        /// <param name="color">the fill color</param>
        public FillColor(Color color)
            : base(float.MaxValue, float.MaxValue)
        {
            Color = color;
        }
        /// <summary>
        /// Mehtod to evaluate the color at the given position
        /// </summary>
        /// <param name="position">the position in local space of the fill</param>
        /// <returns>the color at the given point</returns>
        protected override Color GetColorAt(ref Vector2 position)
        {
            return Color;
        }
    }
}
