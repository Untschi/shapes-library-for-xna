﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Shapes.Misc.Appearance
{
    /// <summary>
    /// A Fill Object whith a texture
    /// </summary>
    public sealed class FillTexture : Fill
    {
        Color[] _Colors;

        /// <summary>
        /// Creates a new FillTexture
        /// </summary>
        /// <param name="textureAssetName">the asset name of the texture to use</param>
        /// <remarks>The asset name is used instead of the texture itself to be able to easily save and load the shape from xml data</remarks>
        public FillTexture(string textureAssetName)
            : base(0, 0)
        {
            if (!ShapeTexturePool.IsInitialized)
                throw new ShapeTexturePoolNotInitializedException();

            KeyValuePair<Texture2D, Color[]> tuple = ShapeTexturePool.Load(textureAssetName);
            _Colors = tuple.Value;

            base.Transform._Width = tuple.Key.Width;
            base.Transform._Height = tuple.Key.Height;
        }
        /// <summary>
        /// Mehtod to evaluate the color at the given position
        /// </summary>
        /// <param name="position">the position in local space of the fill</param>
        /// <returns>the color at the given point</returns>
        protected override Color GetColorAt(ref Vector2 position)
        {
            int idx = TextureGenerator.GetPixelIndex((int)position.X, (int)position.Y, (int)Transform.Width, (int)Transform.Height);
            return _Colors[idx];
        }
    }
}
