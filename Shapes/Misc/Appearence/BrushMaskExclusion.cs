﻿using System;
using System.Collections.Generic;
using System.Linq;
/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
using System.Text;
using Microsoft.Xna.Framework;
using Shapes.Geometry;

namespace Shapes.Misc.Appearance
{
    /// <summary>
    /// Excludes excludes the outer or inner side of a shape from the Brush
    /// </summary>
    public class BrushMaskExclusion : BrushMask
    {
        private bool _IsOutsideExcluded;

        /// <summary>
        /// Initializes a new instance of the <see cref="BrushMaskExclusion"/> class.
        /// </summary>
        /// <param name="isOutsideExcluded">if true, the brush is only visible inside of the shape. false does the opposite</param>
        public BrushMaskExclusion(bool isOutsideExcluded)
        {
            _IsOutsideExcluded = isOutsideExcluded;
        }

        internal override void ModifyScaleFactor(ref Vector2 scale)
        {
            if (_IsOutsideExcluded)
                scale *= 0;
        }

        internal override float GetVisibilityValue(Brush brush, Drawing geometry, ref Vector2 localPosition, ref float distance, ref float? t)
        {
            if (!(geometry is Shape))
                return 1;
                //throw new ArgumentException("BrushMaskExclusion works with Shapes only, not drawings.");
            
            bool isInside = (geometry as Shape).IsPointInside(ref localPosition);

            if ((isInside && !_IsOutsideExcluded)
                || (!isInside && _IsOutsideExcluded))
                return 0f;
           
            return 1f;
        }
    }
}
