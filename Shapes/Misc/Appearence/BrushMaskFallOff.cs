﻿/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Geometry;
using Microsoft.Xna.Framework;

namespace Shapes.Misc.Appearance
{
    /// <summary>
    /// A FallOff which makes a brush tip smooth
    /// </summary>
    public class BrushMaskFallOff : BrushMask
    {
        float _FallOff;

        /// <summary>
        /// The FallOff defines, what distance at the outline of the brush tip is used to fade out into full transparency.
        /// </summary>
        public float FallOff { get { return _FallOff; } set { _FallOff = value; } }

        /// <summary>
        /// creates a new FallOff mask
        /// </summary>
        /// <param name="fallOff">The FallOff defines, what distance at the outline of the brush tip is used to fade out into full transparency.</param>
        public BrushMaskFallOff(float fallOff)
        {
            _FallOff = fallOff;
        }

        internal override void ModifyScaleFactor(ref Vector2 scale)
        {
            // don't change the scale value
        }

        internal override float GetVisibilityValue(Brush brush, Drawing geometry, ref Vector2 localPosition, ref float distance, ref float? t)
        {
            if (distance < brush.Radius - _FallOff)
                return 1;


            return MathHelper.Lerp(0, 1, (brush.Radius - distance)/_FallOff);
        }
    }
}
