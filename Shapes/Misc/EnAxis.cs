﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Shapes.Misc
{
    public enum Axis3
    {
        X = 0,
        Y = 1,
        Z = 2,
    }

    public static class VectorExtensions
    {
        public static Vector2 ToVector2 (this Vector3 self, Axis3 axisToDelete)
        {
            float tmp;
            return ToVector2(self, axisToDelete, false, out tmp);
        }
        public static Vector2 ToVector2(this Vector3 self, Axis3 axisToDelete, bool switchAxis, out float deletedValue)
        {
            Vector2 result;
            switch (axisToDelete)
            {
                case Axis3.X:
                    result = (switchAxis) ? new Vector2(self.Z, self.Y) : new Vector2(self.Y, self.Z);
                    deletedValue = self.X;
                    break;
                case Axis3.Y:
                    result = (switchAxis) ? new Vector2(self.Z, self.X) : new Vector2(self.X, self.Z);
                    deletedValue = self.Y;
                    break;
                case Axis3.Z:
                    result = (switchAxis) ? new Vector2(self.Y, self.X) : new Vector2(self.X, self.Y);
                    deletedValue = self.Z;
                    break;
                default:
                    throw new ArgumentException();
            }
            return result;
        }

        public static Vector3 ToVector3(this Vector2 self, Axis3 axisToAdd)
        {
            return ToVector3(self, axisToAdd, false, 0.0f);
        }
        public static Vector3 ToVector3(this Vector2 self, Axis3 axisToAdd, bool switchSides, float newAxisValue)
        {
            Vector3 result;
            switch (axisToAdd)
            {
                case Axis3.X:
                    result = (switchSides) ? new Vector3(newAxisValue, self.Y, self.X) : new Vector3(newAxisValue, self.X, self.Y);
                    break;
                case Axis3.Y:
                    result = (switchSides) ? new Vector3(self.Y, newAxisValue, self.X) : new Vector3(self.X, newAxisValue, self.Y);
                    break;
                case Axis3.Z:
                    result = (switchSides) ? new Vector3(self.Y, self.X, newAxisValue) : new Vector3(self.X, self.Y, newAxisValue);
                    break;
                default:
                    throw new ArgumentException();
            }
            return result;
        }
    }
}
