﻿/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
#if !ZUNE

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Geometry;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Shapes.Misc.Shading;

namespace Shapes.Misc.Shading
{
    /// <summary>
    /// Batch to draw a shape with a shader
    /// </summary>
    public class ShapeBatch
    {
        private Quadrangle _Quad;
        private Effect _Effect;

        private EffectParameter _Transformation;
        private EffectParameter _Color;

        private GraphicsDevice _GraphicsDevice;


        /// <summary>
        /// Initializes a new instance of the <see cref="ShapeBatch"/> class.
        /// </summary>
        /// <param name="graphicsDevice">The graphics device.</param>
        public ShapeBatch(GraphicsDevice graphicsDevice)
        {
            _GraphicsDevice = graphicsDevice;
            _Quad = Quadrangle.Find(graphicsDevice);
            //_Effect = new Effect(graphicsDevice, QuadEffect.CompiledEffect, CompilerOptions.None, null);
            _Effect = QuadEffect.CreateEffect(graphicsDevice);

            _Transformation = _Effect.Parameters["Transformation"];
            _Color = _Effect.Parameters["Color"];
        }

        /// <summary>
        /// 
        /// </summary>
        public void Begin()
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="transformation"></param>
        public void Begin(Matrix transformation)
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        public void End()
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="shape"></param>
        /// <param name="color"></param>
        public void DrawFill(Shape shape, Color color)
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rect"></param>
        /// <param name="color"></param>
        public void DrawFilledRect(Rect rect, Color color)
        {
            _Transformation.SetValue(GetCompleteTransformation(rect._Transform));
            _Color.SetValue(color.ToVector4());

            _Quad.Draw(_Effect);
        }

        private Matrix GetCompleteTransformation(Transformation2D transform)
        {
            float scX = 1f/_GraphicsDevice.PresentationParameters.BackBufferWidth;
            float scY = 1f/_GraphicsDevice.PresentationParameters.BackBufferHeight;
            //float ar = _GraphicsDevice.DisplayMode.AspectRatio;

            return 
                     Matrix.CreateTranslation(
                                        scX * transform._Origin.X,
                                        scY * transform._Origin.Y,
                                        0)

                   * Matrix.CreateScale(
                                        scX * transform._Scale.X * transform._Width,
                                        scY * transform._Scale.Y * transform._Height,
                                        1)

                   * Matrix.CreateRotationZ(
                                        transform._Rotation.Radians)

                   * Matrix.CreateTranslation(
                                        2 * scX * transform.Position.X - 1, 
                                        2 * scY * -transform.Position.Y + 1, 
                                        0)
                   
                   ;

        }
    }

}
#endif