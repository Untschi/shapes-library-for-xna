﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Shapes.Misc
{
    /// <summary>
    /// this pool holds the texture data for SpriteShapes and FillTextures to save memory if several SpriteShapes / FillTextures are using the same Texture
    /// </summary>
    public static class ShapeTexturePool
    {
        internal static Dictionary<string, KeyValuePair<Texture2D, Color[]>> _Textures = new Dictionary<string, KeyValuePair<Texture2D, Color[]>>();

        /// <summary>
        /// Indicates wether ShapeTexturePool.Initialize() has already been called
        /// </summary>
        public static bool IsInitialized { get { return _Content != null; } }
        static ContentManager _Content;

        /// <summary>
        /// Initializes the Pool to be able to Load Textures
        /// </summary>
        /// <param name="content">the content manager of the game</param>
        public static void Initialize(ContentManager content)
        {
            _Content = content;
        }

        /// <summary>
        /// Clears all texture data
        /// </summary>
        public static void Clear()
        {
            _Textures.Clear();
        }

        internal static KeyValuePair<Texture2D, Color[]> Load(string textureName)
        {
            if (_Textures.ContainsKey(textureName))
                return _Textures[textureName];

            Texture2D tex = _Content.Load<Texture2D>(textureName);
            Color[] col = new Color[tex.Width * tex.Height];
            tex.GetData<Color>(col);

            KeyValuePair<Texture2D, Color[]> tuple = new KeyValuePair<Texture2D, Color[]>(tex, col);
            _Textures.Add(textureName, tuple);

            return tuple;
        }

    }
}
