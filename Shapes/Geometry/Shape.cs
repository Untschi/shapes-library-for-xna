﻿/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
using System;
using System.Text;
using Microsoft.Xna.Framework;

namespace Shapes.Geometry
{
    /// <summary>
    /// a geometry which can be filled
    /// </summary>
    public abstract class Shape : Drawing
    {
        /// <summary>
        /// Base Constructor for Shape
        /// </summary>
        protected Shape()
            : base()
        {
        }

        /// <summary>
        /// a test to check wether a point is inside the Shape
        /// </summary>
        /// <param name="point">the point to check</param>
        /// <returns>true if the point is inside</returns>
        public bool IsPointInside(Vector2 point)
        {
            return IsPointInside(ref point, false);
        }
        /// <summary>
        /// a test to check wether a point is inside the Shape
        /// </summary>
        /// <param name="point">the point to check</param>
        /// <param name="isLocalCoordinate">if true, the given point is handled in local space (will not be transformed). 
        /// otherwise it is handled as parent space (global space, if it isn't attatched to another transform).</param>
        /// <returns>true if the point is inside</returns>
        public bool IsPointInside(Vector2 point, bool isLocalCoordinate)
        {
            return IsPointInside(ref point, isLocalCoordinate);
        }
        private bool IsPointInside(ref Vector2 point, bool isLocalCoordinate)
        {
            if(!isLocalCoordinate)
                _Transform.TransformGlobalToLocal(ref point);

            if (!_Transform.Contains(ref point))
                return false;

            return IsPointInside(ref point);
        }
        /// <summary>
        /// a test to check wether a point is inside the Shape
        /// </summary>
        /// <param name="point">the point to check</param>
        /// <returns>true if the point is inside</returns>
        internal abstract bool IsPointInside(ref Vector2 point);


    }
}
