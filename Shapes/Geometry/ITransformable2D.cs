﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Shapes.Geometry
{
    /// <summary>
    /// Interface which provides direct access to Position, Origin, Scale and Rotation
    /// </summary>
    public interface ITransformable2D
    {
        /// <summary>
        /// The Position
        /// </summary>
        Vector2 Position { get; set; }
        /// <summary>
        /// The Origin
        /// </summary>
        Vector2 Origin { get; set; }
        /// <summary>
        /// The Scale
        /// </summary>
        Vector2 Scale { get; set; }
        /// <summary>
        /// The Rotation
        /// </summary>
        Angle Rotation { get; set; }
    }
}
