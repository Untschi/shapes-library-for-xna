﻿/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Shapes.Geometry
{
       
#if XNA4 || MONO
    using Color = Microsoft.Xna.Framework.Color;
#else
    using Color = Microsoft.Xna.Framework.Graphics.Color;
#endif 

    #region base interface
    /// <summary>
    /// The interface for helper objects to define a SpriteShape
    /// </summary>
    public interface IColorCondition
    {
        /// <summary>
        /// Method is called from the SpriteShape class to check if a pixel is part of the shape or not
        /// </summary>
        /// <param name="color">the color of the pixel to check</param>
        /// <returns>true, if the pixel is part of the SpriteShape</returns>
        bool CheckColorCondition(ref Color color);
        /// <summary>
        /// This method is needed for Serialization to have a pregnant identifier
        /// </summary>
        /// <returns>the name for serialization</returns>
        string GetTypeName();
    }
    #endregion


    // --------------------------------------------------------------------
    // --- ALPHA THRESHOLD
    // --------------------------------------------------------------------

    #region Alpha Threshold
    /// <summary>
    /// An Object To define a SpriteShape by the textures pixel transparency
    /// </summary>
    public struct AlphaThreshold : IColorCondition
    {
        /// <summary>
        /// The Identifier of the Object used for Serialization
        /// </summary>
        public const string TypeName = "AlphaThreshold";

        internal byte AlphaThresholdValue;
        internal bool IsInverted;

        /// <summary>
        /// Creates a new AlphaThreshold object
        /// </summary>
        /// <param name="alphaThreshold">the threshold value indicating from which value on the pixels are part of the shape</param>
        public AlphaThreshold(byte alphaThreshold)
        {
            AlphaThresholdValue = alphaThreshold;
            IsInverted = false;
        }
        /// <summary>
        /// Creates a new AlphaThreshold object
        /// </summary>
        /// <param name="alphaThreshold">the threshold value indicating from which value on the pixels are part of the shape</param>
        /// <param name="isInverted">if true, the opposite pixels are (not) part of the shape</param>
        public AlphaThreshold(byte alphaThreshold, bool isInverted)
        {
            AlphaThresholdValue = alphaThreshold;
            IsInverted = isInverted;
        }

        /// <summary>
        /// Method is called from the SpriteShape class to check if a pixel is part of the shape or not
        /// </summary>
        /// <param name="color">the color of the pixel to check</param>
        /// <returns>true, if the pixel is part of the SpriteShape</returns>
        public bool CheckColorCondition(ref Color color)
        {
            return (color.A >= AlphaThresholdValue && !IsInverted)
                || (color.A <= AlphaThresholdValue && IsInverted);
        }

        /// <summary>
        /// This method is needed for Serialization to have a pregnant identifier
        /// </summary>
        /// <returns>the name for serialization</returns>
        public string GetTypeName()
        {
            return TypeName;
        }
    }
    #endregion


    // --------------------------------------------------------------------
    // --- COLOR MAP
    // --------------------------------------------------------------------

    #region Color Map
    /// <summary>
    /// An Object to define a SpriteShape by a color
    /// </summary>
    public struct ColorMap : IColorCondition
    {
        /// <summary>
        /// The Identifier of the Object used for Serialization
        /// </summary>
        public const string TypeName = "ColorMap";

        internal Color Color;
        internal bool IsInverted;

        /// <summary>
        /// Creates a new ColorMap object
        /// </summary>
        /// <param name="color">the exact color of the pixels which are part of the shape</param>
        public ColorMap(Color color)
        {
            Color = color;
            IsInverted = false;
        }     
        /// <summary>
        /// Creates a new ColorMap object
        /// </summary>
        /// <param name="color">the exact color of the pixels which are part of the shape</param>
        /// <param name="isInverted">if true, the opposite pixels are (not) part of the shape</param>
        public ColorMap(Color color, bool isInverted)
        {
            Color = color;
            IsInverted = isInverted;
        }


        /// <summary>
        /// Method is called from the SpriteShape class to check if a pixel is part of the shape or not
        /// </summary>
        /// <param name="color">the color of the pixel to check</param>
        /// <returns>true, if the pixel is part of the SpriteShape</returns>
        public bool CheckColorCondition(ref Color color)
        {
            return ((color == Color) && !IsInverted)
                || ((color != Color) && IsInverted);
        }

        /// <summary>
        /// This method is needed for Serialization to have a pregnant identifier
        /// </summary>
        /// <returns>the name for serialization</returns>
        public string GetTypeName()
        {
            return TypeName;
        }
    }
    #endregion

}
