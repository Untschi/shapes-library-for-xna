﻿/* HEADER
 * ------
 * © 2009 by Salomon Zwecker 
 * modified by:
 * - 
 */
using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

namespace Shapes.Geometry
{
    /// <summary>
    /// a dot drawing
    /// </summary>
    public sealed class Dot : Drawing
    {
        /// <summary>
        /// creates a dot drawing
        /// </summary>
        public Dot(Vector2 position)
            : base()
        {
            _Transform.Position = position;
        }

        internal override float GetDistanceToEdge(ref Vector2 point)
        {
            return point.Length();
            //return (float)Math.Sqrt((double)((_Bounding._Position.X - point.X) * (_Bounding._Position.X - point.X)
            //    + (_Bounding._Position.Y - point.Y) * (_Bounding._Position.Y - point.Y)));
        }

        internal override Vector2 GetNearestPointOnEdge(ref Vector2 point)
        {
            return Vector2.Zero;
        }

        internal override Vector2 GetPositionFromT(float t)
        {
            return Vector2.Zero;
        }

        internal override float GetEdgePathValueFromPoint(ref Vector2 point)
        {
            return 0;
        }
        internal override Vector2 GetTangent(ref Vector2 position)
        {
            return Vector2.Zero;
        }
        internal override Vector2 GetNormal(ref Vector2 position)
        {
            return Vector2.Zero;
        }
        /// <summary>
        /// Clones the Dot.
        /// </summary>
        /// <returns>an object with the type Dot</returns>
        public override object Clone()
        {
            Dot d = new Dot(Position);
            d._Transform = (Transformation2D)_Transform.Clone();
            return d;
        }

        /// <summary>
        /// Gets the enumeration type which is mapped to this kind of object
        /// </summary>
        /// <returns>the geometry typee of this object</returns>
        public override GeometryType GetGeometryType()
        {
            return GeometryType.Dot;
        }

        public override LineStrip ToLineStrip()
        {
            return new LineStrip(
                Origin + new Vector2(this.Position.X - 1, this.Position.Y),
                Origin + new Vector2(this.Position.X, this.Position.Y - 1),
                Origin + new Vector2(this.Position.X + 1, this.Position.Y),
                Origin + new Vector2(this.Position.X, this.Position.Y + 1)) 
                { IsClosed = true, Origin = new Vector2(1, 1), Rotation = this.Rotation, Scale = this.Scale };
        }
    }
}
