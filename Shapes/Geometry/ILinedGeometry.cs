﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Shapes.Geometry
{
    /// <summary>
    /// Interface providing a Line Enumerator and a transformation
    /// </summary>
    public interface ILinedGeometry
    {
        /// <summary>
        /// Iterates through all Lines of the object
        /// </summary>
        /// <returns>every line the object contains</returns>
        IEnumerable<Line> GetLines();
        /// <summary>
        /// The global position of the first lines start point
        /// </summary>
        Vector2 StartPoint { get; }
        /// <summary>
        /// The Transformation of the geometry
        /// </summary>
        Transformation2D Transform { get; }

        int LineCount { get; }
    }

    /// <summary>
    /// Interface derives from ILinedGeometry. It is for solid geometry which is defined by lines
    /// </summary>
    public interface ILinedShape : ILinedGeometry
    {
        /// <summary>
        /// Checks wether a given point is inside the shape or not
        /// </summary>
        /// <param name="point">the point to check in global space</param>
        /// <returns>true, if the point is inside the shape</returns>
        bool IsPointInside(Vector2 point);
    }
}
