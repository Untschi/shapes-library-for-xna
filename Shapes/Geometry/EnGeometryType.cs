﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Shapes.Geometry
{
    /// <summary>
    /// An Enumeration mapping to the different kinds of geometry the shapes library supports
    /// </summary>
    public enum GeometryType 
    {
        /// <summary> </summary>
        Unknown = 0,

        /// <summary> </summary>
        Dot = 10,
        /// <summary> </summary>
        Line = 11,
        /// <summary> </summary>
        LineStrip = 12,
        /// <summary> </summary>
        SplineSegment = 13,
        /// <summary></summary>
        Spline = 14,
        /// <summary></summary>
        LineList = 15,

        /// <summary>
        /// a Circle is an ellipse with two radia of the same length
        /// </summary>
        /// <remarks>leave the circle the first Shape in the enum</remarks>
        Circle = 20,
        /// <summary>
        /// an Ellipse can be a Circle
        /// </summary>
        Ellipse = 21,
        /// <summary> </summary>
        Rect = 22,
        /// <summary> </summary>
        Triangle = 23,
        /// <summary> </summary>
        Polygon = 24,
        /// <summary> </summary>
        SplinePoly = 25,
        /// <summary> </summary>
        SpriteShape = 26,

        /// <summary> </summary>
        ShapeComposition = 50,
    }

    public static class GeometryTypeExtensions
    {
        /// <summary>
        /// returns true, if the value stands for a Drawing
        /// </summary>
        public static bool IsDrawing(this GeometryType self)
        {
            return self != GeometryType.Unknown && (int)self < (int)GeometryType.Circle;
        }

        /// <summary>
        /// returns true, if the value stands for a Shape
        /// </summary>
        public static bool IsShape(this GeometryType self)
        {
            return (int)self >= (int)GeometryType.Circle;
        }
    }
}
