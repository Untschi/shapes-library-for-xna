﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sampler;
using Shapes.Misc;
using Microsoft.Xna.Framework.Graphics;
using Shapes.Misc.Appearance;
using Microsoft.Xna.Framework.Input;
using Shapes.Geometry;
using Microsoft.Xna.Framework;
using ShapesExtensions;

namespace TestGame.Samples
{
    /// <summary>
    /// The ColorMapSample shows how to create SpriteShapes from a Texture by searching for a specified color.
    /// ColorMaps can be very useful e.g. for Point'n'Click Adventures. 
    /// You could paint all clickable things in one graphic and create a shape for each of them.
    /// If you have something which disappears later (a collectable item for example), 
    /// you can also paint it to the graphic and only remove the shape for it.
    /// </summary>
    public class ColorMapSample : Sample
    {
        // the generator to create textures from shapes
        TextureGenerator _Generator;

        // a helper which lets control the active shape with any input
        // look into the HelperClasses Folder to see how this class works
        TestGame.HelperClasses.CursorHelper _Cursor;

        // the shapes for the different parts of the picture
        SpriteShape _DeerBlue, _DeerYellow, _DeerRed, _Forest;
        
        // a sprite for the picture itself and it's color map
        Sprite _Art, _ColorMap;

        // the sprite for the mouse cursor
        GeometrySprite _CursorSprite;

        // the text which is displayed next to the cursor
        string _CursorText = "";


        public ColorMapSample(TextureGenerator generator)
            : base("ColorMap Sample:\n"
            + "Move the cursor over the picture to discover the different parts.\n"
            + "Press any Cursor-Button to show / hide the color map\n"
            + "(Painting by Franz Marc: \"Rehe im Wald II\" / \"Deer in the Forest II\")")
        {
            _Generator = generator;
            _Cursor = new TestGame.HelperClasses.CursorHelper(Game.GraphicsDevice);
            _Cursor.ButtonReleased += _Cursor_ButtonReleased;
        }



        protected override void Initialize()
        {
            // the brush for the cursor shape graphic
            Brush brush = Brush.CreateFallOffBrush(1, 1);
            brush.Color = Color.Black;

            // Create the Cursor Graphic
            _CursorSprite = new GeometrySprite(
                    CustomTemplates.GetMouseCurser(), _Generator, new FillColor(Color.White), brush
            );

            // load the two textures
            Texture2D artTex = Game.Content.Load<Texture2D>("deer");
            Texture2D colorMapTex = Game.Content.Load<Texture2D>("deer_colormap");

            // make sprites from the textures (Helper-classes: not inside the shapes library)
            _Art = new Sprite(artTex, new Vector2(150, 65));
            _ColorMap = new Sprite(colorMapTex, _Art.Transform.Position);
            _ColorMap.IsVisible = false;

            // create ColorMap-SpriteShapes for every part
            _DeerBlue   = new SpriteShape("deer_colormap", new ColorMap(new Color(0, 0, 255)));
            _DeerRed    = new SpriteShape("deer_colormap", new ColorMap(new Color(255, 0, 0)));
            _DeerYellow = new SpriteShape("deer_colormap", new ColorMap(new Color(255, 255, 0)));
            _Forest     = new SpriteShape("deer_colormap", new ColorMap(new Color(0, 255, 0)));

            // set all to the same position
            _Forest.Position = 
            _DeerBlue.Position = 
            _DeerRed.Position = 
            _DeerYellow.Position = _Art.Transform.Position;
        }

        public override void Update(float seconds, 
            GamePadState currentButtons, GamePadState previousButtons, 
            KeyboardState currentKeys, KeyboardState previousKeys, 
            MouseState currentMouse, MouseState previousMouse)
        {
            // update Cursor
            _Cursor.Update(seconds, currentButtons, previousButtons, currentKeys, previousKeys, currentMouse, previousMouse);
            
            // set cursor sprite to mouse position
            _CursorSprite.Geometry.Transform.Position = _Cursor.Position;

            // check all the ColorMap-SpriteShapes if the mouse is hovering over one of them
            if (_Forest.IsPointInside(_Cursor.Position))
            {
                _CursorText = "some forest";
            }
            else if (_DeerBlue.IsPointInside(_Cursor.Position))
            {
                _CursorText = "blue father deer";
            }
            else if (_DeerRed.IsPointInside(_Cursor.Position))
            {
                _CursorText = "red mother deer";
            }
            else if (_DeerYellow.IsPointInside(_Cursor.Position))
            {
                _CursorText = "yellow deer kid";
            }
            else
            {
                _CursorText = "";
            }
        }
        // this method is called when the CursorHelper class detects that a cursor-button has been released
        void _Cursor_ButtonReleased(TestGame.HelperClasses.CursorButton button)
        {
            // invert the visibility of both graphics
            _ColorMap.IsVisible = !_ColorMap.IsVisible;
            _Art.IsVisible = !_Art.IsVisible;
        }

        public override void Draw(Microsoft.Xna.Framework.Graphics.SpriteBatch batch)
        {
            Game.GraphicsDevice.Clear(Color.Black);

            batch.Begin();

            // Draw all Sprites (they handle internally if they are visible or not)
            _Art.Draw(batch);
            _ColorMap.Draw(batch);
            _CursorSprite.Draw(batch);
            
            // write text next to the cursor
            SpriteFont font = SampleManager.Instance.Font;
            batch.DrawString(font, _CursorText, _CursorSprite.Geometry.Transform.Position + new Vector2(20, 20), Color.White);

            batch.End();
        }
    }
}
