﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

using Sampler;
using Shapes.Misc;
using Shapes.Geometry;

namespace TestGame.Samples
{
    /// <summary>
    /// This class is reserved for you to test the library by yourself.
    /// Don't forget to add it to the SampleManager in the Game1.Initialize() method
    /// </summary>
    public class Sandbox : Sample
    {
        private TextureGenerator _Generator;


        public Sandbox(TextureGenerator generator)
            : base("Sandbox:\n"
            + "...")
        {
            _Generator = generator;
        }


        // this method is called, when the sample is activated the first time
        protected override void Initialize()
        {
        }

        protected override void DeActivate()
        {
            // you may delete the sample's content here.
            // if you do so, call the following Method:

            //ReInitializeOnNextActivation();
        }


        public override void Update(float seconds,
                                GamePadState currentButtons, GamePadState previousButtons,
                                KeyboardState currentKeys, KeyboardState previousKeys,
                                MouseState currentMouse, MouseState previousMouse)
        {
        }

        public override void Draw(SpriteBatch batch)
        {
            Game.GraphicsDevice.Clear(Color.CornflowerBlue);

            batch.Begin();
           
            // ...

            batch.End();
        }

        
    }
}
