﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sampler;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Shapes.Geometry;
using Shapes.Misc.Shading;
using Shapes.Misc;


namespace TestGame.Samples
{
    class ShaderSample : Sample
    {
        TextureGenerator generator;
        ShapeBatch shapeBatch;
        Rect rect;
        Texture2D rectTex;

        public ShaderSample(TextureGenerator generator)
            : base("Shader")
        {
            this.generator = generator;
        }

        protected override void Initialize()
        {
            rect = new Rect(30, 50);
            rect.Position = new Vector2(100, 100);
            shapeBatch = new ShapeBatch(generator.GraphicsDevice);

            rectTex = generator.GeometryFilled(rect);
        }

        public override void Update(float seconds, GamePadState currentButtons, GamePadState previousButtons, KeyboardState currentKeys, Microsoft.Xna.Framework.Input.KeyboardState previousKeys, MouseState currentMouse, MouseState previousMouse)
        {
            rect.Rotation += seconds;
        }

        public override void Draw(SpriteBatch batch)
        {
            shapeBatch.DrawFilledRect(rect, Color.Red);
            batch.Draw(rectTex, new Vector2(200, 100), null, Color.Yellow, rect.Rotation.ClockwiseRadians, rect.Origin, rect.Scale, SpriteEffects.None, 0);
        }
    }
}
