﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shapes.Geometry;
using Microsoft.Xna.Framework.Graphics;
using Shapes.Misc;

namespace ShapesExtensions
{
    /// <summary>
    /// A CollisionSprite is a Texture which has a Shape connected to it. They use the same transformation.
    /// </summary>
    public class CollisionSprite : Sprite
    {
        public Shape Geometry { get { return geometry; } }
        Shape geometry;

        Texture2D shapeTexture;

        /// <summary>
        /// Creates a new collision sprite
        /// </summary>
        /// <param name="texture">the texture of the sprite</param>
        /// <param name="geometry">the shape which is connected to the texture</param>
        public CollisionSprite(Texture2D texture, Shape geometry)
            : this(texture, geometry, null)
        { }

        /// <summary>
        /// Creates a new collision sprite
        /// </summary>
        /// <param name="texture">the texture of the sprite</param>
        /// <param name="geometry">the shape which is connected to the texture</param>
        /// <param name="ShapeTextureGenerator">pass a TextureGenerator if you wanna see the shape as well</param>
        public CollisionSprite(Texture2D texture, Shape geometry, TextureGenerator ShapeTextureGenerator)
            : base(texture)
        {
            this.geometry = geometry;
            base._Transform = geometry.Transform;

            if (ShapeTextureGenerator != null)
            {
                shapeTexture = ShapeTextureGenerator.GeometryFilled(geometry);
            }
        }

        public void SetupCollision(CollisionDetector2D detector, bool isActive, bool isPassive)
        {
            detector.SetupGeometry(geometry, isActive, isPassive);
        }

        /// <summary>
        /// Draws the sprite.
        /// </summary>
        /// <param name="batch">The SpriteBatch object ehich is used to draw.</param>
        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);

            if (shapeTexture != null)
            {
#if XNA4
                batch.Draw(shapeTexture, _Transform.Position, SourceRectangle, Color * 0.5f,
                    _Transform.Rotation.ClockwiseRadians, _Transform.Origin, _Transform.Scale, Flip, LayerDepth); 
#else
                batch.Draw(shapeTexture, _Transform.Position, SourceRectangle, new Color(Color, 128),
                    _Transform.Rotation.ClockwiseRadians, _Transform.Origin, _Transform.Scale, Flip, LayerDepth);

#endif
            }
        }
    }
}
