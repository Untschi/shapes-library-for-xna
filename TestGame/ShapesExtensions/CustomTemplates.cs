﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Shapes.Misc;
using Shapes.Geometry;

namespace ShapesExtensions
{
    /// <summary>
    /// Some Custom Shapes for the samples
    /// </summary>
    public static class CustomTemplates
    {

        public static Polygon GetMouseCurser()
        {
            return new Polygon(GeometryTemplates.CreateArrow(new Vector2(15, 15), new Vector2(0, 0), 5, 15, 12));
        }

    }
}
