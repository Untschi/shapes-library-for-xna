using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

// use these:
using Sampler;
using TestGame.Samples;
using Shapes.Misc;
using Shapes.Geometry;

namespace Sampler.ShapeSample
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // The SampleManager is needed to create Samples and update and draw them.
        SampleManager _SampleManager;
   
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
#if XNA4
            Content.RootDirectory = "TestGame.Xna.4.0";
#elif XNA_3_0
            Content.RootDirectory = "Content.Xna3.0";
#else
            Content.RootDirectory = "Content";
#endif
        }

        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice); int w = GraphicsDevice.Viewport.Width;

            // if you ever want to use any SpriteShape you have to initialize the pool first
            ShapeTexturePool.Initialize(Content);

            // one TextureGenerator is enough for all samples.
            TextureGenerator textureGenerator = new TextureGenerator(GraphicsDevice);

            // a font for the description text
            SpriteFont font = Content.Load<SpriteFont>("LucidaConsole");
            // create the SampleManager first
            _SampleManager = new SampleManager(this, graphics, font);

#if !ZUNE // Default is Page Up/Down. uncomment this, if you have a small keyboard without those keys.
            _SampleManager.NextSampleKey = Keys.Tab;
            _SampleManager.PreviousSampleKey = Keys.Back;
#endif

            // then you can create the samples. Add them to the SampleManager.            
            _SampleManager.Add(new CollisionSample1(textureGenerator));
            _SampleManager.Add(new ColorMapSample(textureGenerator));
            _SampleManager.Add(new ShapeCompositionSample(textureGenerator));
            _SampleManager.Add(new LineStripEditingSample(textureGenerator));
            _SampleManager.Add(new JumpNRunSample(textureGenerator));
           // _SampleManager.Add(new ShaderSample(textureGenerator));

            // these two samples are also for the zune / phone:
            _SampleManager.Add(new PathSample(textureGenerator));
            _SampleManager.Add(new CollisionSample2(textureGenerator));

            // _SampleManager.Add(new Sandbox(textureGenerator)); // <<-- reserved for you. 

            // after all samples are added, the SampleManager must be activated.
            _SampleManager.Activate();
            _SampleManager.ManagerTextColor = Color.DarkTurquoise;

        }


        protected override void Update(GameTime gameTime)
        {
#if !ZUNE
            // Allows the game to exit
            if (Keyboard.GetState().IsKeyDown(Keys.Escape) ||
                GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
#endif

            // don't forget to update the SampleManger
            _SampleManager.Update(gameTime);

            base.Update(gameTime);
        }


        protected override void Draw(GameTime gameTime)
        {
            // Let the SampleManager draw the active sample
            _SampleManager.Draw(gameTime, spriteBatch);
            
            base.Draw(gameTime);
        }


    }
}
